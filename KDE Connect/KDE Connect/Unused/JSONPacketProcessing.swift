//
//  JSONPacketProcessing.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-07-10.
//

//import Foundation
//
//struct GenericJSONPacket : Codable {
//    let id: Int
//    let type: String
//    let body: String
//    let tcpPort: Int
//}
//
//struct IdentityJSONPacket : Codable {
//    let deviceId: String
//    let deviceName: String
//    let protocolVersion: Int
//    let deviceType: String
//    let incomingCapabilities: [String]
//    let outgoingCapabilities: [String]
//}
